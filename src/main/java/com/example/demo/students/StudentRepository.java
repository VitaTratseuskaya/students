package com.example.demo.students;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<StudentEntity, Long> {

    Optional<StudentEntity> findById(Long id);

    @Query(nativeQuery = true, value = "SELECT * FROM students WHERE course_id = :courseId")
    List<StudentEntity> findAllStudentsByCourseId(@Param("courseId") long courseId);

    @Query(nativeQuery = true, value = "SELECT * FROM students_subjects WHERE subject_id = :subjectId")
    List<Long> findAllStudentsBySubjectId(@Param("subjectId") Long subjectId);

}
