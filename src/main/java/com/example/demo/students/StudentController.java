package com.example.demo.students;

import com.example.demo.students.dto.*;
import com.example.demo.utils.IsSuccessAnswer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Api(tags = "Student Controller", description = "Запросы для работы с учениками/студентами")
public class StudentController {

    private final StudentService studentService;

    @PostMapping("/createStudent")
    @ApiOperation(value = "Запрос на создание студента", response = StudentFullDto.class)
    public StudentFullDto createStudent(@RequestBody StudentCreateDto student) {
        return studentService.createStudent(student);
    }

    @PostMapping("/addSubjectToStudent/{studentId}/ByName/{subjectName}")
    @ApiOperation(value = "Запрос на добавление предмета студенту", response = StudentWithSubjects.class)
    public StudentWithSubjects addSubjectByName(@PathVariable Long studentId, @PathVariable String subjectName) {
        return studentService.addSubjectByName(studentId, subjectName);
    }

    @PutMapping("/updateStudent")
    @ApiOperation(value = "Запрос на обновление студента", response = StudentUpdateDto.class)
    public StudentUpdateDto updateDto(@RequestBody StudentUpdateDto student) {
        return studentService.updateStudent(student);
    }

    @GetMapping("/findAllStudentsByCourseId/{courseId}")
    @ApiOperation(value = "Запрос на поиск всех студентов на курсе", response = StudentFullDto.class, responseContainer = "List")
    public List<StudentFullDto> findAllStudentsByCourseId(@PathVariable long courseId) {
        return studentService.findAllStudentsByCourseId(courseId);
    }

    @GetMapping("/findById/{id}")
    @ApiOperation(value = "Запрос на поиск студента по ID", response = StudentFullDto.class)
    public StudentFullDto findByStudentsId(@PathVariable Long id) {
        return studentService.findByStudentId(id);
    }

    @GetMapping("/findAllStudents")
    @ApiOperation(value = "Запрос на поиск всех студентов", response = StudentFullDto.class, responseContainer = "List")
    public List<StudentEntity> allStudents() {
        return studentService.allStudents();
    }

    @GetMapping("/findAllStudentsBySubjectId/{subjectId}")
    @ApiOperation(value = "Запрос на поиск всех студентов по предмету", response = StudentShortDto.class, responseContainer = "List")
    public List<StudentShortDto> findAllStudentsBySubjectId(@PathVariable Long subjectId) {
        return studentService.findAllStudentsBySubjectId(subjectId);
    }

    @PostMapping("/deleteCourseByStudentId/{studentId}")
    @ApiOperation(value = "Запрос на удаление курса у студента", response = StudentShortDto.class)
    public StudentShortDto deleteCourseByStudentId(@PathVariable Long studentId) {
        return studentService.deleteCourseByStudentId(studentId);
    }

    @DeleteMapping("/deleteSubject/{subjectName}/fromStudent/{studentId}")
    @ApiOperation(value = "Запрос на удаление предмета у студента", response = StudentWithSubjects.class)
    public StudentWithSubjects deleteSubjectFromStudentId(@PathVariable String subjectName, @PathVariable Long studentId) {
        return studentService.deleteSubjectFromStudentsList(subjectName, studentId);
    }

    @DeleteMapping(value = "/deleteStudentById/{id}")
    @ApiOperation(value = "Запрос на удаление студента", response = IsSuccessAnswer.class)
    public IsSuccessAnswer deleteStudentById(@PathVariable Long id) {
        return studentService.deleteStudentById(id);
    }

}
