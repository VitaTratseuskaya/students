package com.example.demo.students;

import com.example.demo.course.CourseEntity;
import com.example.demo.students.dto.*;
import com.example.demo.subjects.SubjectEntity;
import com.example.demo.subjects.SubjectRepository;
import com.example.demo.subjects.dto.SubjectShortDto;
import com.example.demo.utils.IsSuccessAnswer;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class StudentService {

    private final Faker faker = new Faker();
    private final StudentRepository studentRepository;
    private final SubjectRepository subjectRepository;

    public StudentFullDto createStudent(StudentCreateDto student) {
        StudentEntity createStudent = new StudentEntity();
        StudentFullDto resultStudent = new StudentFullDto();

        createStudent.setFirstName(faker.funnyName().name());
        createStudent.setSurname(faker.name().lastName());
        createStudent.setAddress(faker.address().streetAddress());
        createStudent.setBirthDate(student.getBirthDate());
        createStudent.setMobilePhone(faker.phoneNumber().cellPhone());
        createStudent.setEmail(faker.internet().emailAddress());
        createStudent.setPassword(faker.numerify(String.valueOf(Math.random() * 8)));

        studentRepository.save(createStudent);
        log.info("Student was successfully created");

        resultStudent.setFirstName(createStudent.getFirstName());
        resultStudent.setSurname(createStudent.getSurname());
        resultStudent.setAddress(createStudent.getAddress());
        resultStudent.setBirthDate(createStudent.getBirthDate());
        resultStudent.setMobilePhone(createStudent.getMobilePhone());
        resultStudent.setEmail(createStudent.getEmail());
        resultStudent.setPassword(createStudent.getPassword());

        return resultStudent;
    }

    public StudentWithSubjects addSubjectByName(Long studentId, String subjectName) {
        SubjectEntity subjectEntity = subjectRepository.findByNameSubject(subjectName);
        StudentEntity studentEntity = studentRepository.getOne(studentId);

        List<SubjectEntity> listSubjects = studentEntity.getSubjects();
        listSubjects.add(subjectEntity);
        for (int i = 0; i < listSubjects.size(); i++) {
            SubjectEntity sub = listSubjects.get(i);
            if (subjectEntity.getSubjectId().equals(sub.getSubjectId())) {
                System.out.println("You have this subject in your list already!");
            } else {
                studentEntity.setSubjects(listSubjects);
                System.out.println("The subject was successfully added!");
                break;
            }
        }

        List<SubjectShortDto> listSubjectShortDto = new ArrayList<>();
        for (SubjectEntity s : listSubjects) {
            SubjectShortDto subjectShortDto = new SubjectShortDto();
            subjectShortDto.setSubjectId(s.getSubjectId());
            subjectShortDto.setSubjectName(s.getSubjectName());
            listSubjectShortDto.add(subjectShortDto);
        }

        StudentWithSubjects studentWithSubjects = new StudentWithSubjects();
        studentWithSubjects.setStudentId(studentEntity.getStudentId());
        studentWithSubjects.setFirstName(studentEntity.getFirstName());
        studentWithSubjects.setSurname(studentEntity.getSurname());
        studentWithSubjects.setSubjectId(listSubjectShortDto);
        try {
            studentWithSubjects.setCourseId(studentEntity.getCourse().getCourseId());
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        return studentWithSubjects;
    }

    public StudentFullDto updateStudent(StudentUpdateDto student) {
        StudentEntity updateStudent = new StudentEntity();
        StudentFullDto resultStudent = new StudentFullDto();

        updateStudent.setStudentId(student.getStudentId());
        updateStudent.setFirstName(student.getFirstName());
        updateStudent.setSurname(student.getSurname());
        updateStudent.setAddress(student.getAddress());
        updateStudent.setBirthDate(student.getBirthDate());
        updateStudent.setMobilePhone(student.getMobilePhone());
        updateStudent.setEmail(student.getEmail());
        updateStudent.setPassword(student.getPassword());

        studentRepository.save(updateStudent);

        resultStudent.setStudentId(updateStudent.getStudentId());
        resultStudent.setFirstName(updateStudent.getFirstName());
        resultStudent.setSurname(updateStudent.getSurname());
        resultStudent.setAddress(updateStudent.getAddress());
        resultStudent.setBirthDate(updateStudent.getBirthDate());
        resultStudent.setMobilePhone(updateStudent.getMobilePhone());
        resultStudent.setEmail(updateStudent.getEmail());
        resultStudent.setPassword(updateStudent.getPassword());

        return resultStudent;
    }

    public StudentFullDto findByStudentId(Long id) {
        StudentEntity student = studentRepository
                .findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND) {
                });

        StudentFullDto result = new StudentFullDto();
        result.setStudentId(student.getStudentId());
        result.setFirstName(student.getFirstName());
        result.setSurname(student.getSurname());
        result.setAddress(student.getAddress());
        result.setBirthDate(student.getBirthDate());
        result.setMobilePhone(student.getMobilePhone());
        result.setEmail(student.getEmail());
        result.setPassword(student.getPassword());

        try {
            result.setCourseId(student.getCourse().getCourseId());
        } catch (Exception ex) {
            result.setCourseId(null);
        }

        return result;
    }

    public List<StudentEntity> allStudents() {
        return studentRepository.findAll();
    }

    public List<StudentFullDto> findAllStudentsByCourseId(Long courseId) {
        List<StudentFullDto> result = new ArrayList<>();

        List<StudentEntity> listStudents = studentRepository.findAllStudentsByCourseId(courseId);
        for (StudentEntity s : listStudents) {
            StudentFullDto studentFullDto = new StudentFullDto();
            studentFullDto.setCourseId(s.getCourse().getCourseId());
            studentFullDto.setStudentId(s.getStudentId());
            studentFullDto.setAddress(s.getAddress());
            studentFullDto.setEmail(s.getEmail());
            studentFullDto.setBirthDate(s.getBirthDate());
            studentFullDto.setFirstName(s.getFirstName());
            studentFullDto.setMobilePhone(s.getMobilePhone());
            studentFullDto.setPassword(s.getPassword());
            studentFullDto.setSurname(s.getSurname());

            result.add(studentFullDto);
        }

        return result;
    }

    public List<StudentShortDto> findAllStudentsBySubjectId(Long subjectId) {
        List<StudentShortDto> result = new ArrayList<>();

        List<Long> listStudents = studentRepository.findAllStudentsBySubjectId(subjectId);
        for (Long s : listStudents) {
            StudentEntity studentEntity = studentRepository.getOne(s);

            StudentShortDto studentShortDto = new StudentShortDto();
            studentShortDto.setCourseId(studentEntity.getCourse().getCourseId());
            studentShortDto.setStudentId(studentEntity.getStudentId());
            studentShortDto.setFirstName(studentEntity.getFirstName());
            studentShortDto.setSurname(studentEntity.getSurname());

            result.add(studentShortDto);
        }

        return result;
    }

    public StudentShortDto deleteCourseByStudentId(Long studentId) {
        StudentEntity studentEntity = studentRepository.getOne(studentId);
        studentEntity.setCourse(null);

        StudentShortDto studentShortDto = new StudentShortDto();
        studentShortDto.setStudentId(studentEntity.getStudentId());
        try {
        studentShortDto.setCourseId(studentEntity.getCourse().getCourseId());
        } catch (NullPointerException ex) {
            System.out.println("CourseId set null");
        }

        studentShortDto.setSurname(studentEntity.getSurname());
        studentShortDto.setFirstName(studentEntity.getFirstName());

        return studentShortDto;
    }

    public StudentWithSubjects deleteSubjectFromStudentsList(String subjectName, Long studentId) {
        StudentEntity studentEntity = studentRepository.getOne(studentId);

        List<SubjectEntity> listSubjects = studentEntity.getSubjects();
        for (int i = 0; i < listSubjects.size(); i++) {
            SubjectEntity sub = listSubjects.get(i);
            if (sub.getSubjectName().equals(subjectName)) {
                listSubjects.remove(sub);
                System.out.println("Subject: " + subjectName + " was successfully deleted from list");
                break;
            } else {
                System.out.println("You haven't this subject in your list");
            }
        }

        List<SubjectShortDto> listSubjectShortDto = new ArrayList<>();
        for (SubjectEntity s : listSubjects) {
            SubjectShortDto subjectShortDto = new SubjectShortDto();
            subjectShortDto.setSubjectId(s.getSubjectId());
            subjectShortDto.setSubjectName(s.getSubjectName());
            listSubjectShortDto.add(subjectShortDto);
        }

        StudentWithSubjects studentWithSubjects = new StudentWithSubjects();
        studentWithSubjects.setStudentId(studentEntity.getStudentId());
        studentWithSubjects.setFirstName(studentEntity.getFirstName());
        studentWithSubjects.setSurname(studentEntity.getSurname());
        studentWithSubjects.setCourseId(studentEntity.getCourse().getCourseId());
        studentWithSubjects.setSubjectId(listSubjectShortDto);
        return studentWithSubjects;
    }

    public IsSuccessAnswer deleteStudentById(Long id) {
        IsSuccessAnswer result = new IsSuccessAnswer();
        boolean isSuccess = true;

        try {
            studentRepository.deleteById(id);
            log.info("Student was successfully deleted!");
        } catch (Exception e) {
            isSuccess = false;
            log.info("Student wasn't deleted!");
        }

        result.setIsSuccess(isSuccess);
        return result;
    }

}
