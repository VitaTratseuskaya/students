package com.example.demo.students.dto;

public class StudentShortDto {

    private Long studentId;
    private String firstName;
    private String surname;
    private Long courseId;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    @Override
    public String toString() {
        return "StudentShortDto{" +
                "studentId=" + studentId +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", courseId=" + courseId +
                '}';
    }
}
