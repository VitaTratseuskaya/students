package com.example.demo.students.dto;

import com.example.demo.subjects.dto.SubjectCreateDto;
import com.example.demo.subjects.dto.SubjectShortDto;

import java.util.List;

public class StudentWithSubjects {

    private Long studentId;
    private String firstName;
    private String surname;
    private Long courseId;
    private List<SubjectShortDto> subjectId;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public List<SubjectShortDto> getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(List<SubjectShortDto> subjectId) {
        this.subjectId = subjectId;
    }

    @Override
    public String toString() {
        return "StudentWithSubjects{" +
                "studentId=" + studentId +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", courseId=" + courseId +
                ", subjectId=" + subjectId +
                '}';
    }
}

