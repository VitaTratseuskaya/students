package com.example.demo.rating;

import com.example.demo.rating.dto.RatingFullDto;
import com.example.demo.utils.IsSuccessAnswer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Api(tags = "Rating Controller", description = "Запросы для работы с рейтингом")
public class RatingController {

    private final RatingService ratingService;

    @PostMapping("/addRating")
    @ApiOperation(value = "Запрос для добавления рейтинга", response = RatingFullDto.class, responseContainer = "List")
    public /*List<RatingFullDto>*/void addRatingAll() {
        /*return*/ ratingService.addRatingAll();
    }

    @GetMapping("/ratingFor1Semestr")
    @ApiOperation(value = "Запрос для просмотра рейтинга за 1 семестр", response = RatingFullDto.class, responseContainer = "List")
    public List<RatingFullDto> ratingFor1Semestr() {
        return ratingService.raringFor1Semestr();
    }

    @GetMapping("/ratingFor2Semestr")
    @ApiOperation(value = "Запрос для просмотра рейтинга за 2 семестр", response = RatingFullDto.class, responseContainer = "List")

    public List<RatingFullDto> ratingFor2Semestr() {
        return ratingService.raringFor2Semestr();
    }

    @GetMapping("/ratingFor3Semestr")
    @ApiOperation(value = "Запрос для просмотра рейтинга за 3 семестр", response = RatingFullDto.class, responseContainer = "List")
    public List<RatingFullDto> ratingFor3Semestr() {
        return ratingService.raringFor3Semestr();
    }

    @GetMapping("/viewAllRating")
    @ApiOperation(value = "Запрос для просмотра рейтинга", response = RatingEntity.class, responseContainer = "List")
    public List<RatingEntity> viewAllRating() {
        return ratingService.viewRating();
    }

    @DeleteMapping("/deleteAllRating")
    @ApiOperation(value = "Запрос для удаления рейтинга третьего семестра", response = RatingEntity.class, responseContainer = "List")
    public IsSuccessAnswer deleteAllRating() {
        return ratingService.deleteAllRating();
    }

}
