package com.example.demo.rating;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RatingRepository extends JpaRepository<RatingEntity, Long> {


    @Modifying
    @Query(nativeQuery = true, value = "INSERT INTO rating" +
            "(student_id, alltime_rating, alltime_count, sem_rating1, sem_count1, sem_rating2, sem_count2, sem_rating3, sem_count3) " +
            " VALUES (:studentId, :alltimeRating, :alltimeCount, :semRating1, :semCount1, :semRating2, :semCount2, :semRating3, :semCount3 )")
    void rating(@Param("studentId") Long studentId,
                @Param("alltimeRating") int alltimeRating,
                @Param("alltimeCount") double alltimeCount,
                @Param("semRating1") double semRating1,
                @Param("semCount1") double semCount1,
                @Param("semRating2") double semRating2,
                @Param("semCount2") double semCount2,
                @Param("semRating3") double semRating3,
                @Param("semCount3") double sem_count3);
}
