package com.example.demo.rating;

import com.example.demo.grades.GradeRepository;
import com.example.demo.grades.GradesService;
import com.example.demo.rating.dto.RatingFullDto;
import com.example.demo.students.StudentEntity;
import com.example.demo.students.StudentRepository;
import com.example.demo.utils.IsSuccessAnswer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class RatingService {

    private final RatingRepository ratingRepository;
    private final GradeRepository gradeRepository;
    private final StudentRepository studentRepository;
    private final GradesService gradesService;

    public /*List<RatingFullDto>*/void addRatingAll() {
        List<StudentEntity> students = studentRepository.findAll();
        RatingEntity ratingEntity = new RatingEntity();

        double count1 = 0.0;
        double count2 = 0.0;
        double count3 = 0.0;

        for (StudentEntity stud : students) {
            Long studentId = stud.getStudentId();
            count1 = gradesService.GPAStudentIdFor1Semestr(studentId);
            count2 = gradesService.GPAStudentIdFor1Semestr(studentId);
            count3 = gradesService.GPAStudentIdFor3Semestr(studentId);

            List<Long> grades = gradeRepository.findAllGradesByStudentId(stud.getStudentId());
            double result = 0.0;
            if (!grades.isEmpty()) {
                for(Long i : grades) {
                    result = result + i;
                }
            } else {
                System.out.println("THE LIST GRADES IS EMPTY!!!");
            }

        }

        ratingRepository.rating(ratingEntity.getStudentId(),
                ratingEntity.getAlltimeRating(),
                ratingEntity.getAlltimeCount(),
                ratingEntity.getSemRating1(),
                ratingEntity.getSemCount1(),
                ratingEntity.getSemRating2(),
                ratingEntity.getSemCount2(),
                ratingEntity.getSemRating3(),
                ratingEntity.getSemCount3());

       // rating.sort((o1, o2) -> Double.compare (o2.getAlltimeCount(),o1.getAlltimeCount()));
       /* List<StudentEntity> students = studentRepository.findAll();
        List<RatingFullDto> rating = new ArrayList<>();

        for (StudentEntity stud : students) {
            Long st = stud.getStudentId();
            List<Long> grades = gradeRepository.findAllGradesByStudentId(stud.getStudentId());
            double result = 0.0;
            if (!grades.isEmpty()) {
                for(Long i : grades) {
                    result = result + i;
                }
                RatingFullDto ratingFullDto = new RatingFullDto();
                ratingFullDto.setStudentId(st);
                ratingFullDto.setAlltimeCount(result / grades.size());
                ratingFullDto.setAlltimeRating(0);
                rating.add(ratingFullDto);

                RatingEntity ratingEntity = new RatingEntity();
                ratingEntity.setStudentId(ratingFullDto.getStudentId());
                ratingEntity.setAlltimeCount(ratingFullDto.getAlltimeCount());
                ratingEntity.setAlltimeRating(ratingFullDto.getAlltimeRating());

                ratingRepository.save(ratingEntity);
            } else {
               System.out.println("THE LIST GRADES IS EMPTY!!!");
            }
        }

        rating.sort((o1, o2) -> Double.compare (o2.getAlltimeCount(),o1.getAlltimeCount()));
        return rating;*/
    }

    public List<RatingFullDto> raringFor1Semestr () {
        List<StudentEntity> students = studentRepository.findAll();
        List<RatingFullDto> rating = new ArrayList<>();
        for (StudentEntity stud : students) {
            Long studentId = stud.getStudentId();
            List<Long> grades = gradeRepository.findAllGradesStudentIdBy1Semestr(stud.getStudentId());
            double result = 0.0;
            if (!grades.isEmpty()) {
                for(Long i : grades) {
                    result = result + i;
                }
                RatingFullDto ratingFullDto = new RatingFullDto();
                ratingFullDto.setStudentId(studentId);
                ratingFullDto.setSemRating1(0.0);
                ratingFullDto.setSemCount1(result / grades.size());
                rating.add(ratingFullDto);

                RatingEntity ratingEntity = new RatingEntity();
                ratingEntity.setStudentId(ratingFullDto.getStudentId());
                ratingEntity.setAlltimeCount(ratingFullDto.getAlltimeCount());
                ratingEntity.setAlltimeRating(ratingFullDto.getAlltimeRating());

                ratingRepository.save(ratingEntity);
            } else {
                System.out.println("THE LIST GRADES IS EMPTY!!!");
            }
        }
        return rating;
    }

    public List<RatingFullDto> raringFor2Semestr () {
        List<StudentEntity> students = studentRepository.findAll();
        List<RatingFullDto> rating = new ArrayList<>();
        for (StudentEntity stud : students) {
            Long studentId = stud.getStudentId();
            List<Long> grades = gradeRepository.findAllGradesStudentIdBy2Semestr(stud.getStudentId());
            double result = 0.0;
            if (!grades.isEmpty()) {
                for(Long i : grades) {
                    result = result + i;
                }
                RatingFullDto ratingFullDto = new RatingFullDto();
                ratingFullDto.setStudentId(studentId);
                ratingFullDto.setSemRating2(0.0);
                ratingFullDto.setSemCount2(result / grades.size());
                rating.add(ratingFullDto);

                RatingEntity ratingEntity = new RatingEntity();
                ratingEntity.setStudentId(ratingFullDto.getStudentId());
                ratingEntity.setAlltimeCount(ratingFullDto.getAlltimeCount());
                ratingEntity.setAlltimeRating(ratingFullDto.getAlltimeRating());

                ratingRepository.save(ratingEntity);
            } else {
                System.out.println("THE LIST GRADES IS EMPTY!!!");
            }
        }
        return rating;
    }

    public List<RatingFullDto> raringFor3Semestr () {
        List<StudentEntity> students = studentRepository.findAll();
        List<RatingFullDto> rating = new ArrayList<>();
        for (StudentEntity stud : students) {
            Long studentId = stud.getStudentId();
            List<Long> grades = gradeRepository.findAllGradesStudentIdBy3Semestr(stud.getStudentId());
            double result = 0.0;
            if (!grades.isEmpty()) {
                for(Long i : grades) {
                    result = result + i;
                }
                RatingFullDto ratingFullDto = new RatingFullDto();
                ratingFullDto.setStudentId(studentId);
                ratingFullDto.setSemRating3(0.0);
                ratingFullDto.setSemCount3(result / grades.size());
                rating.add(ratingFullDto);

                RatingEntity ratingEntity = new RatingEntity();
                ratingEntity.setStudentId(ratingFullDto.getStudentId());
                ratingEntity.setAlltimeCount(ratingFullDto.getAlltimeCount());
                ratingEntity.setAlltimeRating(ratingFullDto.getAlltimeRating());

                ratingRepository.save(ratingEntity);
            } else {
                System.out.println("THE LIST GRADES IS EMPTY!!!");
            }
        }
        return rating;
    }

    public List<RatingEntity> viewRating() {
        List<RatingEntity> rating = ratingRepository.findAll();
        rating.sort((o1, o2) -> Double.compare(o2.getAlltimeCount(), o1.getAlltimeCount()));
        return rating;
    }

    public IsSuccessAnswer deleteAllRating() {
        IsSuccessAnswer result = new IsSuccessAnswer();
        boolean isSuccess = true;

        try {
            ratingRepository.deleteAll(ratingRepository.findAll());
            log.info("Rating list was successfully deleted!");
        } catch (Exception ex) {
            isSuccess = false;
            log.info("Rating list wasn't deleted!");
        }

        result.setIsSuccess(isSuccess);
        return result;
    }


}

