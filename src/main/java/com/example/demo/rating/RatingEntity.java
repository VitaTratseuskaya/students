package com.example.demo.rating;

import javax.persistence.*;

@Entity
@Table(name = "rating")
public class RatingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ratingId;

    @Column(name = "student_id")
    private Long studentId;

    @Column(name = "alltime_rating")
    private int alltimeRating;

    @Column(name = "alltime_count")
    private double alltimeCount;

    @Column(name = "sem_rating1")
    private Double semRating1;

    @Column(name = "sem_count1")
    private Double semCount1;

    @Column(name = "sem_rating2")
    private Double semRating2;

    @Column(name = "sem_count2")
    private Double semCount2;

    @Column(name = "sem_rating3")
    private Double semRating3;

    @Column(name = "sem_count3")
    private Double semCount3;

    public Long getRatingId() {
        return ratingId;
    }

    public void setRatingId(Long ratingId) {
        this.ratingId = ratingId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public int getAlltimeRating() {
        return alltimeRating;
    }

    public void setAlltimeRating(int alltimeRating) {
        this.alltimeRating = alltimeRating;
    }

    public double getAlltimeCount() {
        return alltimeCount;
    }

    public void setAlltimeCount(double alltimeCount) {
        this.alltimeCount = alltimeCount;
    }

    public Double getSemRating1() {
        return semRating1;
    }

    public void setSemRating1(Double semRating1) {
        this.semRating1 = semRating1;
    }

    public Double getSemCount1() {
        return semCount1;
    }

    public void setSemCount1(Double semCount1) {
        this.semCount1 = semCount1;
    }

    public Double getSemRating2() {
        return semRating2;
    }

    public void setSemRating2(Double semRating2) {
        this.semRating2 = semRating2;
    }

    public Double getSemCount2() {
        return semCount2;
    }

    public void setSemCount2(Double semCount2) {
        this.semCount2 = semCount2;
    }

    public Double getSemRating3() {
        return semRating3;
    }

    public void setSemRating3(Double semRating3) {
        this.semRating3 = semRating3;
    }

    public Double getSemCount3() {
        return semCount3;
    }

    public void setSemCount3(Double semCount3) {
        this.semCount3 = semCount3;
    }

    @Override
    public String toString() {
        return "RatingEntity{" +
                "ratingId=" + ratingId +
                ", studentId=" + studentId +
                ", alltimeRating=" + alltimeRating +
                ", alltimeCount=" + alltimeCount +
                ", semRating1=" + semRating1 +
                ", semCount1=" + semCount1 +
                ", semRating2=" + semRating2 +
                ", semCount2=" + semCount2 +
                ", semRating3=" + semRating3 +
                ", semCount3=" + semCount3 +
                '}';
    }
}
