package com.example.demo.course;

import com.example.demo.course.dto.CourseCreateDto;
import com.example.demo.course.dto.CourseFullDto;
import com.example.demo.course.dto.CourseUpdateDto;
import com.example.demo.students.StudentEntity;
import com.example.demo.students.dto.StudentFullDto;
import com.example.demo.utils.IsSuccessAnswer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Api(tags = "Course Controller", description = "Запросы для работы с курсом")
public class CourseController {

    private final CourseService courseService;

    @PostMapping("/courseCreate")
    @ApiOperation(value = "Запрос создания курса", response = CourseEntity.class)
    public CourseFullDto courseCreate(@RequestBody CourseCreateDto course) {
        return courseService.createCourse(course);
    }

    @PostMapping("/addStudent/{studentId}/toTheCourse/{courseId}")
    @ApiOperation(value = "Запрос добавления курса студенту", response = StudentFullDto.class)
    public StudentFullDto addStudentToTheCourse(@PathVariable Long studentId, @PathVariable Long courseId){
        return courseService.addStudentByIdToTheCourse(studentId, courseId);
    }

    @PutMapping("/courseUpdate")
    @ApiOperation(value = "Запрос обновление курса", response = CourseFullDto.class)
    public CourseFullDto courseUpdate(@RequestBody CourseUpdateDto course) {
        return courseService.updateCourse(course);
    }

    @GetMapping("/findByIdCourse/{id}")
    @ApiOperation(value = "Запрос на поиск курса по ID", response = CourseEntity.class)
    public Optional<CourseEntity> findByIdCourse(@PathVariable Long id) {
        return courseService.findByIdCourse(id);
    }

    @GetMapping("/findAllCourses")
    @ApiOperation(value = "Запрос на поиск всех курсов", response = CourseEntity.class, responseContainer = "List")
    public List<CourseEntity> findAllCourses(){
        return courseService.findAllCourses();
    }

    @DeleteMapping("/deleteCourseById/{id}")
    @ApiOperation(value = "Запрос на удаление курса по ID", response = IsSuccessAnswer.class)
    public IsSuccessAnswer deleteCourseById(@PathVariable Long id) {
        return courseService.deleteCourseById(id);
    }
}
