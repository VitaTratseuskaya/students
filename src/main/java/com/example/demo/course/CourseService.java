package com.example.demo.course;

import com.example.demo.course.dto.CourseCreateDto;
import com.example.demo.course.dto.CourseFullDto;
import com.example.demo.course.dto.CourseUpdateDto;
import com.example.demo.students.StudentEntity;
import com.example.demo.students.StudentRepository;
import com.example.demo.students.dto.StudentFullDto;
import com.example.demo.utils.IsSuccessAnswer;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class CourseService {

    private final Faker faker = new Faker();
    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;

    public CourseFullDto createCourse(CourseCreateDto course) {
        CourseFullDto result = new CourseFullDto();

        CourseEntity courseForCreate = new CourseEntity();
        courseForCreate.setFacultyName(faker.university().name().intern());
        courseForCreate.setCourseTutor(faker.funnyName().name());
        courseForCreate.setDeanOfFaculty(faker.name().fullName());

        courseRepository.save(courseForCreate);
        log.info("Course was successfully created!");

        result.setCourseTutor(courseForCreate.getCourseTutor());
        result.setFacultyName(courseForCreate.getFacultyName());
        result.setDeanOfFaculty(courseForCreate.getDeanOfFaculty());

        return result;
    }

    public CourseFullDto updateCourse(CourseUpdateDto course) {
        CourseFullDto result = new CourseFullDto();

        CourseEntity courseForUpdate = new CourseEntity();
        courseForUpdate.setCourseId(course.getCourseId());
        courseForUpdate.setCourseTutor(course.getCourseTutor());
        courseForUpdate.setDeanOfFaculty(course.getDeanOfFaculty());
        courseForUpdate.setFacultyName(course.getFacultyName());

        courseRepository.save(courseForUpdate);
        log.info("Course was successfully updated!");

        result.setCourseId(courseForUpdate.getCourseId());
        result.setDeanOfFaculty(courseForUpdate.getDeanOfFaculty());
        result.setCourseTutor(courseForUpdate.getCourseTutor());
        result.setFacultyName(courseForUpdate.getFacultyName());

        return result;
    }

    public StudentFullDto addStudentByIdToTheCourse(Long studentId, Long courseId) {
        StudentEntity student = studentRepository.getOne(studentId);
        CourseEntity course = courseRepository.getOne(courseId);
        student.setCourse(course);

        StudentFullDto studentFullDto = new StudentFullDto();
        studentFullDto.setStudentId(student.getStudentId());
        studentFullDto.setFirstName(student.getFirstName());
        studentFullDto.setSurname(student.getSurname());
        studentFullDto.setAddress(student.getAddress());
        studentFullDto.setBirthDate(student.getBirthDate());
        studentFullDto.setMobilePhone(student.getMobilePhone());
        studentFullDto.setEmail(student.getEmail());
        studentFullDto.setPassword(student.getPassword());
        studentFullDto.setCourseId(course.getCourseId());

        return studentFullDto;
    }

    public Optional<CourseEntity> findByIdCourse(Long id) {
        return courseRepository.findById(id);
    }

    public List<CourseEntity> findAllCourses() {
        return courseRepository.findAll();
    }

    public IsSuccessAnswer deleteCourseById(Long id) {
        IsSuccessAnswer result = new IsSuccessAnswer();
        boolean isSuccess = true;

        try{
            courseRepository.deleteById(id);
            log.info("Course was successfully deleted!");
        }catch (Exception ex){
            isSuccess = false;
            log.info("Course wasn't deleted!");
        }

        result.setIsSuccess(isSuccess);
        return result;
    }

}
