package com.example.demo.course;

import com.example.demo.students.StudentEntity;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "courses")
public class CourseEntity {

    @Id
    @Column(name = "course_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long courseId;

    @Column(name = "faculty_name")
    private String facultyName;

    @Column(name = "dean_of_faculty")
    private String deanOfFaculty;

    @Column(name = "course_Tutor")
    private String courseTutor;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
    private List<StudentEntity> students;

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getDeanOfFaculty() {
        return deanOfFaculty;
    }

    public void setDeanOfFaculty(String deanOfFaculty) {
        this.deanOfFaculty = deanOfFaculty;
    }

    public String getCourseTutor() {
        return courseTutor;
    }

    public void setCourseTutor(String courseTutor) {
        this.courseTutor = courseTutor;
    }

    public List<StudentEntity> getStudents() {
        return students;
    }

    public void setStudents(List<StudentEntity> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "CourseEntity{" +
                "courseId=" + courseId +
                ", facultyName='" + facultyName + '\'' +
                ", deanOfFaculty='" + deanOfFaculty + '\'' +
                ", courseTutor='" + courseTutor + '\'' +
                ", students=" + students +
                '}';
    }
}
