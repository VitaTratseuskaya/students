package com.example.demo.course.dto;

import java.util.List;

public class CourseFullDto {

    private Long courseId;
    private String facultyName;
    private String courseTutor;
    private String deanOfFaculty;
    private List<Long> studentId;

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getCourseTutor() {
        return courseTutor;
    }

    public void setCourseTutor(String courseTutor) {
        this.courseTutor = courseTutor;
    }

    public String getDeanOfFaculty() {
        return deanOfFaculty;
    }

    public void setDeanOfFaculty(String deanOfFaculty) {
        this.deanOfFaculty = deanOfFaculty;
    }

    public List<Long> getStudentId() {
        return studentId;
    }

    public void setStudentId(List<Long> studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return "CourseFullDto{" +
                "courseId=" + courseId +
                ", facultyName='" + facultyName + '\'' +
                ", courseTutor='" + courseTutor + '\'' +
                ", deanOfFaculty='" + deanOfFaculty + '\'' +
                ", studentId=" + studentId +
                '}';
    }
}
