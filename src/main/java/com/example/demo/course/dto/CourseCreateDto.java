package com.example.demo.course.dto;

public class CourseCreateDto {

    private String facultyName;
    private String deanOfFaculty;
    private String courseTutor;

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getDeanOfFaculty() {
        return deanOfFaculty;
    }

    public void setDeanOfFaculty(String deanOfFaculty) {
        this.deanOfFaculty = deanOfFaculty;
    }

    public String getCourseTutor() {
        return courseTutor;
    }

    public void setCourseTutor(String courseTutor) {
        this.courseTutor = courseTutor;
    }

    @Override
    public String toString() {
        return "CourseCreateDto{" +
                ", facultyName='" + facultyName + '\'' +
                ", deanOfFaculty='" + deanOfFaculty + '\'' +
                ", courseTutor='" + courseTutor + '\'' +
                '}';
    }
}
