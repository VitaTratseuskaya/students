package com.example.demo.course.dto;

public class CourseUpdateDto extends CourseCreateDto {

    private Long courseId;

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    @Override
    public String toString() {
        return "CourseUpdateDto{" +
                "courseId=" + courseId +
                '}';
    }
}
