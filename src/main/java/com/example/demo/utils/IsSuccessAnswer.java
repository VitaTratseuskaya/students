package com.example.demo.utils;

import lombok.Data;

@Data
public class IsSuccessAnswer {

    private Boolean isSuccess;
}