package com.example.demo.subjects;

import com.example.demo.students.StudentEntity;
import com.example.demo.students.StudentRepository;
import com.example.demo.students.dto.StudentFullDto;
import com.example.demo.subjects.dto.SubjectCreateDto;
import com.example.demo.subjects.dto.SubjectFullDto;
import com.example.demo.subjects.dto.SubjectShortDto;
import com.example.demo.subjects.dto.SubjectUpdateDto;
import com.example.demo.utils.IsSuccessAnswer;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class SubjectService {

    private final Faker faker = new Faker();
    private final SubjectRepository subjectRepository;
    private final StudentRepository studentRepository;

    public SubjectFullDto createSubject(SubjectCreateDto createDto) {
        SubjectEntity subject = new SubjectEntity();
        subject.setSubjectName(faker.commerce().productName());

        subjectRepository.save(subject);
        log.info("Subject was successfully created");

        SubjectFullDto result = new SubjectFullDto();
        result.setSubjectName(subject.getSubjectName());

        return result;
    }

    public SubjectFullDto updateSubject(SubjectUpdateDto updateDto) {
        SubjectEntity subject = new SubjectEntity();
        SubjectFullDto subjectUpdateDto = new SubjectFullDto();

        subject.setSubjectId(updateDto.getSubjectId());
        subject.setSubjectName(updateDto.getSubjectName());

        subjectRepository.save(subject);
        log.info("Subject was successfully updated");

        subjectUpdateDto.setSubjectId(subject.getSubjectId());
        subjectUpdateDto.setSubjectName(subject.getSubjectName());

        return subjectUpdateDto;
    }

    public SubjectFullDto findBySubjectId(Long id) {
        SubjectEntity subject = subjectRepository
                .findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND) {
                });

        SubjectFullDto result = new SubjectFullDto();
        result.setSubjectName(subject.getSubjectName());
        result.setSubjectId(subject.getSubjectId());

        return result;
    }

    public ResponseEntity<?> findByNameSubject(String name) {
        SubjectEntity subject = subjectRepository.findByNameSubject(name);
        SubjectFullDto result = new SubjectFullDto();
        if(subject == null) {
            return new ResponseEntity<>("NOT FOUND",HttpStatus.NOT_FOUND);
        }
        result.setSubjectName(subject.getSubjectName());
        result.setSubjectId(subject.getSubjectId());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public List<SubjectEntity> findAllSubjects(){
        return subjectRepository.findAll();
    }

    public List<SubjectShortDto> findAllSubjectsByStudentId(Long studentId){
        StudentEntity studentEntity = studentRepository.getOne(studentId);
        List<SubjectEntity> listSubjects = studentEntity.getSubjects();

        List<SubjectShortDto> listSubjectShortDto = new ArrayList<>();

        for (SubjectEntity s : listSubjects) {
            SubjectShortDto subjectShortDto = new SubjectShortDto();
            subjectShortDto.setSubjectId(s.getSubjectId());
            subjectShortDto.setSubjectName(s.getSubjectName());
            listSubjectShortDto.add(subjectShortDto);
        }
        return listSubjectShortDto;
    }

    public IsSuccessAnswer deleteBySubjectId(Long id) {
        IsSuccessAnswer result = new IsSuccessAnswer();
        boolean isSuccess = true;

        try {
            subjectRepository.deleteById(id);
            log.info("Subject was successfully deleted!");
        } catch (Exception e) {
            isSuccess = false;
            log.info("Subject wasn't deleted!");
        }

        result.setIsSuccess(isSuccess);
        return result;
    }

}
