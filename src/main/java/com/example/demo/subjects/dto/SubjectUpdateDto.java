package com.example.demo.subjects.dto;

public class SubjectUpdateDto extends SubjectCreateDto {

    private Long subjectId;

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    @Override
    public String toString() {
        return "SubjectUpdateDto{" +
                "subjectId=" + subjectId +
                '}';
    }
}
