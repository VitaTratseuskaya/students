package com.example.demo.subjects.dto;

public class SubjectCreateDto {

    private String subjectName;

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @Override
    public String toString() {
        return "SubjectCreateDto{" +
                "subjectName='" + subjectName + '\'' +
                '}';
    }
}
