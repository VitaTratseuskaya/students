package com.example.demo.subjects;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SubjectRepository extends JpaRepository<SubjectEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM subjects WHERE subject_name = :name")
    SubjectEntity findByNameSubject(@Param("name") String name);

}
