package com.example.demo.subjects;

import com.example.demo.subjects.dto.SubjectCreateDto;
import com.example.demo.subjects.dto.SubjectFullDto;
import com.example.demo.subjects.dto.SubjectShortDto;
import com.example.demo.subjects.dto.SubjectUpdateDto;
import com.example.demo.utils.IsSuccessAnswer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@Api(tags = "Subject Controller", description = "Запросы для работы с учебными предметами")
public class SubjectController {

    private final SubjectService subjectService;

    @PostMapping("/createSubject")
    @ApiOperation(value = "Запрос для создания предмета", response = SubjectFullDto.class)
    public SubjectFullDto createSubject(@RequestBody SubjectCreateDto createDto) {
        return subjectService.createSubject(createDto);
    }

    @PutMapping("/updateSubject")
    @ApiOperation(value = "Запрос для обновления предмета", response = SubjectFullDto.class)
    public SubjectFullDto updateSubject(@RequestBody SubjectUpdateDto updateDto) {
        return subjectService.updateSubject(updateDto);
    }

    @GetMapping("/findBySubjectId/{id}")
    @ApiOperation(value = "Запрос для поиска предмета по ID", response = SubjectFullDto.class)
    public SubjectFullDto findBySubjectId(@PathVariable Long id) {
        return subjectService.findBySubjectId(id);
    }

    @GetMapping("/findByNameSubject/{name}")
    @ApiOperation(value = "Запрос для поиска предмета по названию", response = SubjectShortDto.class)
    public ResponseEntity<?> findByNameSubject(@PathVariable String name){
        return subjectService.findByNameSubject(name);
    }

    @GetMapping("/findAllSubjects")
    @ApiOperation(value = "Запрос для поиска всех предметов", response = SubjectEntity.class, responseContainer = "List")
    public List<SubjectEntity> findAllSubjects(){
        return subjectService.findAllSubjects();
    }

    @GetMapping("findAllSubjectsByStudentId/{studentId}")
    @ApiOperation(value = "Запрос для поиска всех предметов у студента", response = SubjectShortDto.class, responseContainer = "List")
    public List<SubjectShortDto> findAllSubjectsByStudentId(@PathVariable Long studentId) {
        return subjectService.findAllSubjectsByStudentId(studentId);
    }

    @DeleteMapping("/deleteBySubjectId/{id}")
    @ApiOperation(value = "Запрос для удаления предмета", response = IsSuccessAnswer.class)
    public IsSuccessAnswer deleteBySubjectId(@PathVariable Long id) {
        return subjectService.deleteBySubjectId(id);
    }
}
