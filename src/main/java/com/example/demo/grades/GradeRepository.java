package com.example.demo.grades;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface GradeRepository extends JpaRepository<GradeEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM `student's`.grades " +
            "LEFT JOIN subjects ON `student's`.grades.subject_id = subjects.subject_id " +
            "WHERE subject_name = :subjectName AND student_id = :studentId AND add_date = :addDate")
    GradeEntity findStudentsGradeByDate(@Param("studentId") Long studentId,
                                        @Param("subjectName") String subjectName,
                                        @Param("addDate") Date addDate);

    @Query(nativeQuery = true, value = "SELECT grade FROM `student's`.grades LEFT JOIN subjects ON `student's`.grades.subject_id = subjects.subject_id " +
            "WHERE subject_name = :subjectName AND student_id = :studentId")
    List<Integer> findAllSubjectsGradesByStudentId(@Param("subjectName") String subjectName,
                                                   @Param("studentId") Long studentId);

    @Modifying
    @Query(nativeQuery = true, value = "DELETE FROM grades WHERE student_id = :studentId AND subject_id = :subjectId AND add_date = :addDate")
    void removeGrade(@Param("studentId") Long studentId,
                     @Param("subjectId") Long subjectId,
                     @Param("addDate") Date date);

    @Query(nativeQuery = true, value = "SELECT grade FROM grades " +
            "LEFT JOIN subjects ON grades.subject_id = subjects.subject_id " +
            "WHERE subject_name = :subjectName AND student_id = :studentId AND (add_date < :finishDate AND add_date > :startDate);")
    List<Integer> subjectsGPAByDates(@Param("studentId") Long studentId,
                                    @Param("subjectName") String subjectName,
                                    @Param("startDate") Date startDate,
                                    @Param("finishDate") Date finishDate);

    @Query(nativeQuery = true, value = "SELECT grade FROM grades WHERE student_id = :studentId")
    List<Long> findAllGradesByStudentId(@Param("studentId") Long studentId);

    @Query(nativeQuery = true, value = "SELECT grade FROM `student's`.grades " +
            "LEFT JOIN subjects ON `student's`.grades.subject_id = subjects.subject_id " +
            "WHERE student_id = :studentId AND (add_date > '2023-09-01' AND add_date < '2023-11-27')")
    List<Long> findAllGradesStudentIdBy1Semestr(@Param("studentId") Long studentId);

    @Query(nativeQuery = true, value = "SELECT grade FROM `student's`.grades " +
            "LEFT JOIN subjects ON `student's`.grades.subject_id = subjects.subject_id " +
            "WHERE student_id = :studentId AND (add_date > '2023-12-03' AND add_date < '2024-01-03')")
    List<Long> findAllGradesStudentIdBy2Semestr(@Param("studentId") Long studentId);

    @Query(nativeQuery = true, value = "SELECT grade FROM `student's`.grades " +
            "LEFT JOIN subjects ON `student's`.grades.subject_id = subjects.subject_id " +
            "WHERE student_id = :studentId AND (add_date > '2023-03-09' AND add_date < '2023-05-31')")
    List<Long> findAllGradesStudentIdBy3Semestr(@Param("studentId") Long studentId);

}
