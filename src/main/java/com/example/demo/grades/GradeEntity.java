package com.example.demo.grades;

import com.example.demo.subjects.SubjectEntity;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.sql.Date;
import javax.persistence.*;

@Entity
@Table(name = "grades")
public class GradeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long gradeId;

   /* @Column(name = "subject_id")
    private Long subjectId;*/

    @Column(name = "grade")
    private int grade;

    @Column(name = "add_date")
    private Date addDate;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name = "subject_id")
    private SubjectEntity subject;

    @Column(name = "student_id")
    private Long studentId;

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }


    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    public SubjectEntity getSubject() {
        return subject;
    }

    public void setSubject(SubjectEntity subject) {
        this.subject = subject;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return "GradeEntity{" +
                "gradeId=" + gradeId +
                ", grade=" + grade +
                ", addDate=" + addDate +
                ", subject=" + subject +
                ", studentId=" + studentId +
                '}';
    }
}
