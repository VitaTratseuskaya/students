package com.example.demo.grades;

import com.example.demo.grades.dto.GradeCreateDto;
import com.example.demo.grades.dto.GradeFullDto;
import com.example.demo.rating.RatingEntity;
import com.example.demo.students.StudentEntity;
import com.example.demo.students.StudentRepository;
import com.example.demo.subjects.SubjectEntity;
import com.example.demo.subjects.SubjectRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class GradesService {

    private final Faker faker = new Faker();
    private final GradeRepository gradeRepository;
    private final SubjectRepository subjectRepository;
    private final StudentRepository studentRepository;

    public GradeFullDto createGrade(GradeCreateDto createDto) {
        GradeEntity gradeEntity = new GradeEntity();
        gradeEntity.setGrade(faker.number().numberBetween(1, 10));
        gradeEntity.setAddDate(createDto.getAddDate());
        gradeEntity.setSubject(subjectRepository.getOne(createDto.getSubjectId()));
        gradeEntity.setStudentId(createDto.getStudentId());

        StudentEntity student = studentRepository.getOne(createDto.getStudentId());

        GradeFullDto gradeFullDto = new GradeFullDto();

        if (student.getCourse() == null) {
            log.info("The student ID " + student.getStudentId() + " doesn't have a course!");
        } else {
            SubjectEntity subject = subjectRepository.getOne(createDto.getSubjectId());
            GradeEntity findGrade = gradeRepository.findStudentsGradeByDate(createDto.getStudentId(), subject.getSubjectName(), createDto.getAddDate());

            if (findGrade == null) {
                gradeRepository.save(gradeEntity);
                log.info("Grade was successfully created");
            } else {
                log.info("You have a grade on this day!");
            }


            gradeFullDto.setGradeId(gradeEntity.getGradeId());
            gradeFullDto.setAddDate(gradeEntity.getAddDate());
            gradeFullDto.setSubjectId(gradeEntity.getSubject().getSubjectId());
            gradeFullDto.setStudentId(gradeEntity.getStudentId());
            gradeFullDto.setGrade(gradeEntity.getGrade());
        }
        return gradeFullDto;
    }

    public Integer findStudentsGradeByDate(Long studentId, String subjectName, Date date) {
        /*StudentEntity studentEntity = studentRepository.getOne(studentId);
          System.out.println("-> " + subjectName);
        List<SubjectEntity> subjectsList = studentEntity.getSubjects();
        for (SubjectEntity subj : subjectsList) {
            if (subj.getSubjectName().equals(subjectName)) {
                List<GradeEntity> gradesList = subj.getGrades();
                for (GradeEntity gr : gradesList) {
                    if (gr.getAddDate().equals(date)) {
                        findGrade = gr.getGrade();
                        System.out.println("Your grade is " + findGrade);
                        break;
                    } else {
                        System.out.println("The grades not found!");
                    }
                    break;
                }
            } else {
                System.out.println("The subject '" + subj.getSubjectName() + "' from this student -> " + studentId + " not found");
                break;
            }
            break;
        }*/
        GradeEntity gradeEntity = gradeRepository.findStudentsGradeByDate(studentId, subjectName, date);
        Integer result = gradeEntity.getGrade();
        log.info("\nStudent: " + studentId + "; \nSubject: " + subjectName + "; \nYour grade: " + result + "; \nDate: " + date);
        return result;
    }

    public Double subjectsGPA(Long studentId, String subjectName) {
        List<Integer> grades = gradeRepository.findAllSubjectsGradesByStudentId(subjectName, studentId);

        double result = 0.0;
        for (Integer i : grades) {
            result = result + i;
        }
        log.info("Your GPA " + result / grades.size() + "; Subject: " + subjectName);
        return result / grades.size();
    }

    public Double GPAStudentIdFor1Semestr(Long studentId){
        List<Long> grades = gradeRepository.findAllGradesStudentIdBy1Semestr(studentId);

        double result = 0.0;
        for (Long i : grades) {
            result = result + i;
        }
        log.info("Your GPA " + result / grades.size() + " for 1 semestr.");
        return result / grades.size();
    }

    public Double GPAStudentIdFor2Semestr(Long studentId){
        List<Long> grades = gradeRepository.findAllGradesStudentIdBy2Semestr(studentId);

        double result = 0.0;
        for (Long i : grades) {
            result = result + i;
        }
        log.info("Your GPA " + result / grades.size() + " for 2 semestr.");
        return result / grades.size();
    }

    public Double GPAStudentIdFor3Semestr(Long studentId){
        List<Long> grades = gradeRepository.findAllGradesStudentIdBy3Semestr(studentId);

        double result = 0.0;
        for (Long i : grades) {
            result = result + i;
        }
        log.info("Your GPA " + result / grades.size() + " for 3 semestr.");
        return result / grades.size();
    }

    public Double subjectsGPAByDates(Long studentId, String subjectName, Date startDate, Date finishDate) {
        List<Integer> gradesByDates = gradeRepository.subjectsGPAByDates(studentId, subjectName, startDate, finishDate);
        double result = 0.0;
        for (Integer i : gradesByDates) {
            result = result + i;
        }
        log.info("\nYour GPA " + result / gradesByDates.size() + "; Subject: " + subjectName + "; \nPeriod -> " + startDate + "-" + finishDate);
        return result / gradesByDates.size();
    }

    public GradeFullDto findByGradeId(Long gradeId) {
        GradeEntity gradeEntity = gradeRepository.getOne(gradeId);

        GradeFullDto gradeFullDto = new GradeFullDto();
        gradeFullDto.setGradeId(gradeEntity.getGradeId());
        gradeFullDto.setAddDate(gradeEntity.getAddDate());
        gradeFullDto.setSubjectId(gradeEntity.getSubject().getSubjectId());
        gradeFullDto.setStudentId(gradeEntity.getStudentId());
        gradeFullDto.setGrade(gradeEntity.getGrade());
        System.out.println("Grade -> " + gradeFullDto);
        return gradeFullDto;
    }

    public List<Integer> findAllSubjectsGradesByStudentId(String subjectName, Long studentId) {
        List<Integer> grades = gradeRepository.findAllSubjectsGradesByStudentId(subjectName, studentId);
        System.out.println("Subject: " + subjectName + "; Student: " + studentId + "\nGrades: " + grades);
        return grades;
    }

    public List<Long> findAllGradesByStudentId(Long studentId) {
        return gradeRepository.findAllGradesByStudentId(studentId);
    }

    public List<Long> findGradesForStudentIdBy1Semestr(Long studentId) {
        return gradeRepository.findAllGradesStudentIdBy1Semestr(studentId);
    }

    public List<Long> findGradesForStudentIdBy2Semestr(Long studentId) {
        return gradeRepository.findAllGradesStudentIdBy2Semestr(studentId);
    }

    public List<Long> findGradesForStudentIdBy3Semestr(Long studentId) {
        return gradeRepository.findAllGradesStudentIdBy3Semestr(studentId);
    }

    public void removeGrade(String subjectName, Long studentId, Date date) {
        SubjectEntity subject = subjectRepository.findByNameSubject(subjectName);
        gradeRepository.removeGrade(studentId, subject.getSubjectId(), date);

    }


}
