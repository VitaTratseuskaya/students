package com.example.demo.grades;

import com.example.demo.grades.dto.GradeCreateDto;
import com.example.demo.grades.dto.GradeFullDto;
import com.example.demo.rating.RatingEntity;
import com.example.demo.utils.IsSuccessAnswer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Api(tags = "Grades Controller", description = "Запросы для работы с отметками учащихся")
public class GradesController {

    private final GradesService gradesService;

    @PostMapping("/createGrade")
    @ApiOperation(value = "Запрос для создания отментки", response = GradeFullDto.class)
    public GradeFullDto createGrade(@RequestBody GradeCreateDto createDto) {
        return gradesService.createGrade(createDto);
    }

    @GetMapping("/findByGradeId/{gradeId}")
    @ApiOperation(value = "Запрос для поиска отметки по ID", response = GradeFullDto.class)
    public GradeFullDto findByGradeId(Long gradeId) {
        return gradesService.findByGradeId(gradeId);
    }

    @GetMapping("/findStudetns/{studentId}/grade/{subjectName}/byDate/{date}")
    @ApiOperation(value = "Запрос для поиска отметок студента в определенный день", response = Integer.class)
    public Integer findStudentsGradeByDate(@PathVariable Long studentId, @PathVariable String subjectName, @PathVariable Date date) {
        return gradesService.findStudentsGradeByDate(studentId, subjectName, date);
    }

    @GetMapping("/subjectsGPA/{subjectName}/byStudentId/{studentId}")
    @ApiOperation(value = "Запрос для поиска среднего балла у студента по предмету", response = Double.class)
    public Double subjectsGPA(@PathVariable Long studentId, @PathVariable String subjectName) {
        return gradesService.subjectsGPA(studentId, subjectName);
    }

    @GetMapping("/GPAFor1semestrByStudentId/{studentId}")
    @ApiOperation(value = "Запрос для поиска среднего балла в первом семестре у студента", response = Double.class)
    public Double GPAFor1SemestrByStudentId(Long studentId) {
        return gradesService.GPAStudentIdFor1Semestr(studentId);
    }

    @GetMapping("/GPAFor2semestrByStudentId/{studentId}")
    @ApiOperation(value = "Запрос для поиска среднего балла во втором семестре у студента", response = Double.class)
    public Double GPAFor2SemestrByStudentId(Long studentId) {
        return gradesService.GPAStudentIdFor2Semestr(studentId);
    }

    @GetMapping("/GPAFor3semestrByStudentId/{studentId}")
    @ApiOperation(value = "Запрос для поиска среднего балла в третьем семестре у студента", response = Double.class)
    public Double GPAFor3SemestrByStudentId(Long studentId) {
        return gradesService.GPAStudentIdFor3Semestr(studentId);
    }

    @GetMapping("/subjectGPA/{subjectName}/byStudentId/{studentId}/dateStart/{startDate}/dateFinish/{finishDate}")
    @ApiOperation(value = "Запрос для поиска среднего балла у студента по предмету за определенный период", response = Double.class)
    public Double subjectsGPAByDates (@PathVariable Long studentId, @PathVariable String subjectName,
                                      @PathVariable Date startDate, @PathVariable Date finishDate){
        return gradesService.subjectsGPAByDates(studentId, subjectName, startDate, finishDate);
    }

    @GetMapping("/findAllGradesByStudentId/{studentId}")
    @ApiOperation(value = "Запрос для поиска всех отметок студента", response = Long.class, responseContainer = "List")
    public List<Long> findAllGradesByStudentId(@PathVariable Long studentId){
        return gradesService.findAllGradesByStudentId(studentId);
    }

    @GetMapping("/findGradesForStudentIdBy1Semestr")
    @ApiOperation(value = "Запрос для просмотра отметок первого семестра у студента", response = RatingEntity.class, responseContainer = "List")
    public List<Long> findGradesForStudentIdBy1Semestr(Long studentId) {
        return gradesService.findGradesForStudentIdBy1Semestr(studentId);
    }

    @GetMapping("/findGradesForStudentIdBy2Semestr")
    @ApiOperation(value = "Запрос для просмотра отметок второго семестра у студента", response = RatingEntity.class, responseContainer = "List")
    public List<Long> findGradesForStudentIdBy2Semestr(Long studentId) {
        return gradesService.findGradesForStudentIdBy2Semestr(studentId);
    }

    @GetMapping("/findGradesForStudentIdBy3Semestr")
    @ApiOperation(value = "Запрос для просмотра отметок третьего семестра у студента", response = RatingEntity.class, responseContainer = "List")
    public List<Long> findGradesForStudentIdBy3Semestr(Long studentId) {
        return gradesService.findGradesForStudentIdBy3Semestr(studentId);
    }

    @GetMapping("/findAllSubjects/{subjectName}/gradesByStudentId/{studentId}")
    @ApiOperation(value = "Запрос для поиска всех отметок студента по предмету", response = Integer.class, responseContainer = "List")
    public List<Integer> findAllSubjectsGradesByStudentId(@PathVariable String subjectName, @PathVariable Long studentId) {
        return gradesService.findAllSubjectsGradesByStudentId(subjectName, studentId);
    }

    @PostMapping("/removeGradeByDate/{date}/{studentId}/{subjectName}")
    @ApiOperation(value = "Запрос для удаления отметки в определенный день", response = Integer.class)
    public void removeGrade(String subjectName, Long studentId, Date date) {
        gradesService.removeGrade(subjectName, studentId, date);
    }
}
