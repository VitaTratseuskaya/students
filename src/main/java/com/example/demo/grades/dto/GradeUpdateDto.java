package com.example.demo.grades.dto;

public class GradeUpdateDto {

    private int grade;

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "GradeUpdateDto{" +
                "grade=" + grade +
                '}';
    }
}
