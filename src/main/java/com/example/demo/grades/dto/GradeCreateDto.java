package com.example.demo.grades.dto;

import java.sql.Date;

public class GradeCreateDto {

    private Long subjectId;
    private Long studentId;
    private int grade;
    private Date addDate;

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    @Override
    public String toString() {
        return "GradeCreateDto{" +
                "subjectId=" + subjectId +
                ", studentId=" + studentId +
                ", grade=" + grade +
                ", addDate=" + addDate +
                '}';
    }
}
